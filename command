#!/usr/bin/env php
<?php

use Core\DB\DB;
use Core\DB\InterfaceMigration\InterfaceMigration;
use Core\DB\Table_Migrations;
use Src\Migrations\Flight;
use Src\Models\Flight as ModelsFlight;

require __DIR__ . '/vendor/autoload.php';
include_once './Core/Path.php';

$var = $argv;
$cmd = explode(':', $var[1]);
if($var[2] ?? false){
    if(strpos($var[2], '-') || strpos($var[2], '--') ){
        echo 'File name could not contain -- or -';
        exit;
    }
}
if(strtolower($cmd[0]) == 'make'){
    if(strtolower($cmd[1]) == 'model'){
        $name = $var[2] ?? null;
        if(!$name || $name === '') {echo 'Please provide migration name!'; exit;}
        $created_path = SRC . '/Models//' . $name;
        if( file_exists($created_path)){
            echo 'File already exist!';
            exit;
        }
        $file = stub(['{name}', '{namespace}'], [pathinfo($name, PATHINFO_FILENAME), trim(str_replace('/', '\\', dirname($name)), '\/')], 'Model_stub.txt');
        writeFile($created_path . '.php', $file);
        echo 'Created successfully!';
        exit;
    } else if(strtolower($cmd[1]) == 'migration') {
        $name = path($var[2]) ?? null;
        if(!$name || $name === '') {echo 'Please provide migration name!'; exit;}
        $tb_name = snakeCase(pathinfo($name, PATHINFO_FILENAME)) . 's';
        $created_path = SRC . '/Migrations//' . time() . '_' . $name;
        $file = stub(['{name}', '{tb_name}'], [pathinfo($name, PATHINFO_FILENAME), $tb_name], 'Migrations_stub.txt');
        writeFile($created_path . '.php', $file);
        echo 'Created successfully!';
        exit;
    } else if (strtolower($cmd[1]) == 'view'){
        $override = false;
        $name = $var[2] ?? null;
        if(!$name || $name === '') {echo 'Please provide view name!'; exit;}
        if(($var[3] ?? false) === '--force'){
            $override = true;
        }
        writeFile(VIEW . '\\' . $var[2] . '.php', '<?php // View generated ?>', $override);  
        echo 'View created successfully!';
        exit;      
    } else if(strtolower($cmd[1]) == 'controller'){
        $name = $var[2] ?? null;
        if($name && $name !== ''){
            try {
                $override = false;
                if(($var[3] ?? false) === '--force'){
                    $override = true;
                }
                $path = CONTROLLER . '/' . trim($name, '\/') . '.php';
                $content = stub(['{name}', '{namespace}'], [pathinfo($name, PATHINFO_FILENAME), trim(str_replace('/', '\\', dirname($name)), '\/')], 'Controller_stub.txt');
                writeFile($path, $content, $override);
                echo 'Controller created!';
                exit;
            } catch (\Exception $e) {
                echo $e->getMessage();
                exit;
            }
        } else {
            echo 'Please provide controller name!';
            exit;
        }
    }
} else if (strtolower($var[1]) == 'migrate') {
    $havent_migrate = true;
    $migrate_err = false;
    include_once CORE.'/DB_config.php';
    $db = new DB();

    $migrated = [];
    
    try{
        include_once CORE . '/DB/TableMigrations.php';
        $default_migrate = $db->isTableExist('table_migrations');
        $db->connect(DB_DRIVER, DB_HOST, DB_NAME, DB_USERNAME, DB_PASSWORD, ['port' => DB_PORT]);
        if(! $default_migrate){
            $conn = $db->getConnection();
            $dm = $conn->prepare(Table_Migrations::migration());
            $dm->execute();
        }else{
            $conn = $db->getConnection();
            $dm = $conn->prepare(Table_Migrations::migratedFile());
            $dm->execute();
            $tmps = $dm->fetchAll(PDO::FETCH_NUM);
            foreach ($tmps as $tmp){
                $migrated[] = $tmp[0];
            }
        }
    } catch (\Exception $e){
        echo $e->getMessage();
        $db->closeDb();
        exit;
    } finally {
        $db->closeDb();
    }
    
    $files = scandir(SRC . '/Migrations');

    // $migrated_path = CORE . '/migrations';
    try {
        $db->connect(DB_DRIVER, DB_HOST, DB_NAME, DB_USERNAME, DB_PASSWORD, ['port' => DB_PORT]);
        foreach($files as $file){
            if($file === '.' || $file === '..'){
                continue;
            }
            if(in_array($file, $migrated)){
                continue;
            }
            $start = microtime(true);
            
            include_once SRC . '/Migrations//' . $file;
            $class_name = explode('.', explode('_', $file)[1])[0];
            $migrate_class = 'Src\Migrations\\'. $class_name;
        
            $conn = $db->getConnection();

            $r = $conn->prepare($migrate_class::migration());
            $r->execute();

            $m = $conn->prepare("INSERT INTO table_migrations (file, migrated_at) VALUES ('$file', '". date('Y-m-d') ."')");
            $m->execute();

            $end = microtime(true);
            echo 'Done! -> ' . ($end - $start) . 's' . PHP_EOL;
            $havent_migrate = false;
            $db->closeDb();
        }
    } catch (\Exception $e){
        echo $e->getMessage();
        $migrate_err = true;
        $db->closeDb();
        exit;
    }

    if($havent_migrate){
        echo 'Nothing to migrate!';
    }
    exit;
} else if (($var[1] ?? false) === 'clear:db') {
    $v2 = $var[2] ?? null;
    if($v2 || $v2 === '') { echo 'Please provide table name!'; exit;}
    $tb_names = explode(',', $var[2]);
    $db = new DB();
    try{
        $db->connect(DB_DRIVER, DB_HOST, DB_NAME, DB_USERNAME, DB_PASSWORD, ['port' => DB_PORT]);
    } catch (\Exception $e){
        echo $e->getMessage();
    } finally {
        $db->closeDb();
        exit;
    }
    
} else {
    echo 'not found command';
}

function writeFile($path, $content, bool $override = false){
    $path = path($path);
    if(file_exists($path) && !$override){
       echo 'File already exist!' . PHP_EOL;
       echo 'If you want to override please add --force';
       exit;
    }
    
    try{
        if(! is_dir(dirname($path))){
            mkdir(dirname($path), 0755, true);
        }
        $stream = fopen($path, 'w');
        fwrite($stream, $content);
        fclose($stream);
    } catch (\Exception $e){
        echo 'Path invalid!';
        exit;
    }
}

// Only create file with stub
function stub(array|string $search, array|string $replace, $stub_name){
    $content = file_get_contents(CORE . '/stub//'. $stub_name);
    return str_replace($search, $replace, $content);
}