<?php // View generated 

use Core\View;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Assignment</title>
    <!-- CSS -->
    <?= view('CSSsheet') ?>
</head>
<body>
    
    <!-- Content body -->
    <?= View::pull_view('test') ?>

    <!-- Script -->
    <?= view('JSscript') ?>
</body>
</html>