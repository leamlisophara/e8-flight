<?php // View generated 

use Core\Route\Route;
use Core\View;
use Src\Models\User;
?>
<?php 
 View::start('test');
?>
    
<div class="container-fluid d-flex justify-content-center align-items-center" style="height: 100vh;">
    <div>
        <div class="text-center">
            <img src="<?= url('/assets/images/e8.jpg') ?>" alt="E8 Logo">
            <div class="text-success" id="default-text" style="font-family: 'Kanit', sans-serif; font-size: 30px;"><?= $text ?? '' ?></div>
        </div>
        <div class="d-flex justify-content-center align-items-center">
            <a href="<?= route('my') ?>" class="btn btn-sm btn-primary">Go To Route My</a>
        </div>
    </div>
</div>


<?php View::end(); ?>
