<?php
namespace Src\Controllers;

use Core\Request;
use Src\Models\Flight;
use Src\Models\User;

class DefaultController {
    // General 
    public function indexAction(Request $q){
        $text = 'Team E8 Assignment';
        $content = view('body', compact('text'));
        return view('default', compact('content'));
    }

    // Show create form
    public function createAction(){

    }

    // Use to store information to db
    public function storeAction(Request $request){
    }

    // Use to show information
    public function showAction(Request $request){

    }

    // Use to show information that need to update
    public function editAction(Request $request){

    }

    // Use to update information
    public function updateAction(Request $request){

    }
}