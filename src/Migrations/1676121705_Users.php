<?php
namespace Src\Migrations;
use Core\DB\Interface\InterfaceMigration;

class Users implements InterfaceMigration{

    public static function migration(): string
    {
        // Raw sql to create alter or drop table
        return "create table users (
            id serial primary key,
            first_name varchar(50),
            last_name varchar(50),
            email varchar(50),
            password varchar(50),
            position varchar(50) default 'visitor',
            created_at date default CURRENT_DATE
        )";
    }
}