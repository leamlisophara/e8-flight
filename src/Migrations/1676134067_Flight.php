<?php
namespace Src\Migrations;
use Core\DB\Interface\InterfaceMigration;

class Flight implements InterfaceMigration{

    public static function migration(): string
    {
        // Raw sql to create alter or drop table
        // recommended table name flights
        return 'create table flights (
            id serial primary key,
            country_name varchar(255),
            country_code int null,
            airport_name varchar(255),
            short_code varchar(255) null   
        )';
    }
}