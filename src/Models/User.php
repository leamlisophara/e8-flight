<?php

namespace Src\Models;

use Core\Model\Model;

class User extends Model{
    // Complete variable data_field to make Model work or 
    // without mission some data
    protected $data_field = [
        'id',
        'first_name',
        'last_name',
        'email',
        'position',
        'created_at',
    ];

}