<?php

namespace Src\Models;

use Core\Model\Model;

class Flight extends Model{
    // Complete variable data_field to make Model work or 
    // without mission some data
    protected $data_field = [
        'country_name',
        'country_code',
        'airport_name',
        'short_code',
    ];

}