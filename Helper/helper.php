<?php

use Core\Config;
use Core\Route\Route;

if(! function_exists('config')){
    function config(string|array $key, $default = null){
        $config = new Config();
        return $config->env($key, $default);
    }
}

if(! function_exists('snakeCase')){
    function snakeCase(string $string){
        return ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $string)), '_');
    }
}

if(! function_exists('view')){
    function view(string $name, array $params = []){
        $names = explode('.', $name);
        $include_path = VIEW . '\\' . implode('\\', $names) . '.php';
        ob_start();
        extract($params);
        include_once $include_path;
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}

if(! function_exists('url')){
    function url(string $path) : string{
        // $http = trim($_SERVER['HTTP_REFERER'], '/');
        $serverName = $_SERVER['HTTP_HOST'] ?? $_SERVER['SERVER_NAME'] ?? config('APP_URL');
        $serverName = stripos('http://', $serverName, 0) !== false || stripos('https://', $serverName, 0) !== false ? $serverName : 'http://' . $serverName;
        $http = trim($serverName , '/');
        $tmp = trim($path, '/');
        return $http . '/' . $tmp;
    }
}

if(! function_exists('include_view')){
    function include_view(string $view, array $args = [], bool $return_value = false){
        try{
            $result = '';
            ob_start();
            extract($args);
            include (VIEW .'/'. trim(path($view), "/\/") .'.php');
            $result = ob_get_contents();
            ob_end_clean();
            if($return_value){
                return $result;
            }
            echo $result;
        } catch (\Exception $e){
            throw new Exception($e->getMessage());
        }
    }
}

if(! function_exists('isRoute')){
    function isRoute($path){
        $uri = $_SERVER['REQUEST_URI'] ?? '';
        return preg_match('/'. trim($path, '/') .'/', trim($uri, '/'));
    }
}

if(! function_exists('route')){
    function route(string $name){
        return Route::route($name);
    }
}

if(! function_exists('path')){
    function path(string $path){
        return str_replace('\\', DIRECTORY_SEPARATOR, $path);
    }
}