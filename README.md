# To run angular to need to install node.js Version >= 18.13
# test add and commit
## To download node 
[Download node TLS version ](https://nodejs.org/en/download/)

#test

## Download angular cli command

```download angular cli
 npm install -g @angular/cli
```

## Run this to install http-server

```install server command
 npm install -g http-server
```

---

## Run command below to install enviroment

```install enviroment
 npm install
```

## Run this before start server

```build angular project
 ng build
```

---

## To create new component you can run

```create component
 ng generate component component_name
```

---

## To start your application server 
 
```start server
 http-server -p 8081 -c-1 dist/front_end
```

---

> Note: command that service will work only for project name with front_end \
>       or you can run follow this syntax ==> http-server -p 8081 -c-1 dist/[project name]


### PHP

## Run create new model

```Create new model
 php command make:model ( model name ) [--force]
```

## Run execute migrations file

```Create new model
 php command make:view ( View name ) [--force]
```

## Run create new migrations file

```Create new migrations file
 php command make:migration ( migrations name ) [--force]
```

## Run execute migrations file

```Migrate all migrations script
 php command migrate
```

> Note () is required but [] is optional \
> <b>$`\textcolor{red}{\text{REMEMBER}}`$</b> --force option will override all your content in exists file