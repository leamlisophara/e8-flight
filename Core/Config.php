<?php
namespace Core;

class Config{

    private $config = [];
    public function __construct()
    {
        $this->config = parse_ini_file(__DIR__ . '/../.env');
    }

    public function env(string|array $key, mixed $default = null){
        $result = $default;
        if(gettype($key) == 'string'){
            $result = $this->config[$key] ?? $default;
        }else if(gettype($key) == 'array'){
            foreach($key as $k){
                array_push($result, $this->config[$k]);
            }
        }
        return $result ?? false;
    }
}