<?php

namespace Core\Auth\Interface;

interface InterfaceAuthentication{

    function authentication(string $username, string $password);

}