<?php 

define('PUBLIC', __DIR__ . '/../public');
define('CORE', __DIR__ );
define('SRC', __DIR__ . '/../src');
define('ROOT', __DIR__ . '/..');
define('VIEW', SRC . '/Views');
define('CONTROLLER', SRC . '/Controllers');
define('ROUTE', SRC . '/Routes');
define('MIGRATION', SRC . '/Migrations');
