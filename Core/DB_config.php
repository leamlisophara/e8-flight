<?php
define('DB_DRIVER', config('DB_DRIVER', 'pgsql'));
define('DB_HOST', config('DB_HOST', '127.0.0.1'));
define('DB_PORT', config('DB_PORT', '5432'));
define('DB_NAME', config('DB_NAME', 'e8_flight'));
define('DB_USERNAME', config('DB_USERNAME', 'root'));
define('DB_PASSWORD', config('DB_PASSWORD', 'root'));
