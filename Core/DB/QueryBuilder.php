<?php 

namespace Core\DB;

use Core\DB\Abstract\AbstractDB;
use Exception;

class QueryBuilder{

    private $table = null;
    private $select = null;
    private $where;
    private $join = '';
    private $onClause;
    private $index;
    private $having;
    private $order;
    
    protected $operator = [
        '=',
        '<',
        '>',
        '<=',
        '>=',
        '<>',
        '=',
        '!=',
        'not',
        'in',
        'is not null',
        'like',
        'ilike',
        'is',
    ];

    public function select($columns = ['*']){
        $select = [];
        foreach($columns as $column){
            array_push($select, $this->replaceAwkward($column));
        }   
        $this->select .= ($this->select === null ? '' : ', ') . implode(', ', $select);
        return $this;
    }

    public function from(string $table){
        $this->table = $table;
        return $this;
    }

    private function condition(string $column, string|int|float $value, string $operator = '=', string $connectionClause = 'and'){
        $where = '';
        if(! in_array($operator, $this->operator)){
            throw new Exception('Unknown Operator ' . $operator);
        }
        $string =  strpos($value, '[', 0) === 0? $value : $this->isNumber($value);
        $where .= ($this->where !== null ? $connectionClause : '') . ' ' . $this->replaceAwkward($column) . ' ' . $this->replaceAwkward($operator) . ' ' . $string;
        return rtrim($where) . ' ';
    }

    public function orWhere(string $column, string|int|float $value, string $operator = '='){
        $this->where .= $this->condition($column, $value, $operator, 'or');
        return $this;
    }

    public function where(string $column, string|int|float $value, string $operator = '='){
        $this->where .= $this->condition($column, $value, $operator);
        return $this;
    }

    public function whereIn(string $column, array $value){
        $v = [];
        foreach($value as $d){
            $v[] = $this->isNumber($d);
        }
        $this->where .= $this->condition($column, "[". implode(',', $v) ."]", 'in');
        return $this;
    }

    public function whereNotNull(string $column){
        $this->where .= $this->condition($column, '', 'is not null');
        return $this;
    }

    public function orWhereNotNull(string $column){
        $this->where .= $this->condition($column, '', 'is not null', 'or');
        return $this;
    }

    public function join(string $table, $callback, string $joinType = 'left'){
        if(!in_array(strtolower($joinType), ['left', 'right', ''])){
            throw new Exception('Incorrect syntax statement '. $joinType . ' join not found!');
        }
        if(!is_callable($callback)){
            throw new Exception('Uncallable method ' . $callback . '!');
        }
        $index = 0;
        $query = new QueryBuilder();
        call_user_func($callback, $query);

        $this->join .= ($joinType ? $joinType . ' ' : '') . 'join ' . $this->replaceAwkward($table) .  ($query->getConditionQuery() ? ' on ' . $query->getConditionQuery() : '') . PHP_EOL;
        return $this;
    }

    public function having(string $column, string $value){
        $this->having .= $this->condition($column, $value, '=');
        return $this;
    }

    public function havingNotNull(string $column){
        $this->having .= $this->condition($column, '', 'is not null', 'and');
        return $this;
    }

    public function orHaving(string $column, string $value){
        $this->having .= $this->condition($column, $value, '=', 'or');
        return $this;
    }

    public function orHavingNotNull(string $column){
        $this->having .= $this->condition($column, '', 'is not null', 'or');
        return $this;
    }

    public function orderBy(string $column, string $direction = 'asc'){
        $this->order .= ($this->order ? ', ' : '') . $column . ' ' . $direction;
        return $this;
    }

    public function getQuery(){
        $query = $this->select && $this->table ? 'select '. $this->select . ' from ' . $this->table . PHP_EOL : '';
        $query .= $this->join;
        $query .= $this->having ?? '';
        $query .= $this->where !== null ? 'where ' . $this->where : ' ';
        $query .= ($this->order ? 'order by ' : '') . $this->order;
        $this->clear();
        return $query;
    }

    private function clear(){
        $this->select = null;
        $this->join = '';
        $this->having = '';
        $this->where = null;
        $this->order = '';
    }

    public function getConditionQuery(){
        return $this->where;
    }

    private function isNumber(string|int|float $value){
        return in_array(gettype($value), ['object', 'string']) && $value != '' ? "'" . $this->replaceAwkward((string)$value) . "'" : $value;
    }

    public function replaceAwkward(string $value){
        $value = strip_tags($value);
        return str_replace([
            '--',
            '//',
            '#',
            '/*',
            '*/',
            '**',
            ';',
            '+',
            '==',
            '||',
        ], '', trim($value));
    }
}