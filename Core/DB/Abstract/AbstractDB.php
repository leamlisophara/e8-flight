<?php

namespace Core\DB\Abstract;

use Core\DB\Interface\InterfaceDB;
use Core\Exception\DB\ExceptionConnectionFail;

abstract class AbstractDB implements InterfaceDB{

    private $connection;

    protected $statement = [
        'select',
        'from',
        'join',
        'on',
        'where',
        'having',
        'group',
        'order by',
    ];

    private $fillable = [
        'driver',
        'host',
        'dbName',
        'username',
        'password'
    ];

    public function connect(string $driver = null, string $host = null, string $dbName = null, string $username = null, string $password = null, $option = [])
    {
        $dbDriver = $driver ? $driver : $this->driver;
        $dbHost = $driver ? $host : $this->host;
        $nameOfDb = $dbName ? $dbName : $this->dbName;
        $dbUsername = $username ? $username : $this->username;
        $dbPassword = $password ? $password : $this->password;

        try{
            $conn = new \PDO("$dbDriver:host=$dbHost;dbname=$nameOfDb", $dbUsername, $dbPassword, $option);
            $this->connection = $conn;
            $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return $this;
        } catch (ExceptionConnectionFail $dbFail) {
            throw new ExceptionConnectionFail($dbFail->getMessage());
        }
    }
    
    public function getConnection(){
        return $this->connection;
    }

    public function __set($name, $value)
    {
        // if(! in_array($name, $this->fillable) && ! in_array($name, $this->statement)){
        //     throw new Exception('Unknown field!');
        // }
        $this->{$name} = $value;
    }

    public function __get($name)
    {
        // if(!in_array($name, $this->fillable) && ! in_array($name, $this->statement)){
        //     throw new Exception('Unknown field!');
        // }
        return $this->{$name} ?? null;
    }

    public function beginTransaction(bool $reopen = false){
        if($reopen){
            $this->connect(DB_DRIVER, DB_HOST, DB_NAME, DB_USERNAME, DB_PASSWORD, ['port' => DB_PORT]);
        }
        return $this->connection->transaction() or die('Can not start transaction!');
    }

    public function commit(){
        $this->connection->commit() or die('Can not commit transaction!');
        $this->connection = null;
    }

    public function rollback(){
        $this->connection->rollback() or die('Can not rollback data!');
        $this->connection = null;
    }

    public function closeDb()
    {
        $this->connection = null;
    }
}