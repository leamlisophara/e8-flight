<?php

namespace Core\DB;

use Core\DB\Abstract\AbstractDB;
use Core\Model\Model;
use ReflectionClass;

include_once __DIR__.'/../DB_config.php';

class DB extends AbstractDB
{
    protected $table;
    private $query;

    public function update(array $arr, $callback){
        if(! is_callable($callback)){
            throw new \Exception("Method $callback not found!");
        }
        $paramVal = [];
        $update = 'UPDATE '. $this->table . ' SET ';
        // $param = '(';
        foreach ($arr as $field => $value){
            // $update .= "'$field', ";
            $update .= "$field = :$field, ";
            $paramVal[":$field"] = $value;
        }
        $update = rtrim($update, ', ') . ' ';
        $builder = new QueryBuilder();
        call_user_func($callback, $builder);
        $query = $update . ' ' . $builder->getQuery();
        return $this->execute($query, $paramVal);
    }

    public function setTable(string $table){
        $this->table = $table;
    }

    public function insert(array $arr){
        $paramVal = [];
        $query = 'INSERT INTO '. $this->table . ' (';
        $param = '( ';
        foreach ($arr as $field => $value){
            $query .= "$field, ";
            $param .= ":$field, ";
            $paramVal[":$field"] = $value;
        }
        $query = rtrim($query, ', ') . ' ) VALUES ' . rtrim($param, ', ') . ' )';
        return $this->execute($query, $paramVal);
    }

    public function delete($callback){
        if(! is_callable($callback)){
            throw new \Exception("Method $callback not found!");
        }
        $builder = new QueryBuilder();
        call_user_func($callback, $builder);

        $query = 'DELETE FROM ' . $this->table . ' ' . $builder->getQuery();
        
        return $this->execute($query);
    }

    public function isTableExist(string $table) : bool{
        $builder = new QueryBuilder();
        $db_driver = config('DB_DRIVER');
        if($db_driver === 'pgsql'){
            $builder->select()->from('pg_tables')->where('schemaname', 'public')->where('tablename', $table);
        }else if($db_driver === 'mysql'){
            $builder->select()->from('information_schema')->where('TABLE_NAME', $table);   
        }
        $result = $this->fetch($builder->getQuery());
        return $result === [] ? false : true;
    }   

    private function fetch(string $query, array $param = []){
        $result = [];
        try{
            $this->connect(DB_DRIVER, DB_HOST, DB_NAME, DB_USERNAME, DB_PASSWORD, ['port' => DB_PORT]);
            $r = $this->getConnection()->prepare($query);
            $r->execute($param);
            $result = $r->fetchAll(\PDO::FETCH_BOTH);
        } catch (\Exception $e){
            var_dump($e->getMessage());
        } finally {
            $this->closeDb();
        }
        return $result;
    }

    private function execute(string $query, array $param = []){
        try{
            $this->connect(DB_DRIVER, DB_HOST, DB_NAME, DB_USERNAME, DB_PASSWORD, ['port' => DB_PORT]);
            return $this->getConnection()->prepare($query)->execute($param); 
        } catch (\Exception $e){
            var_dump($e->getMessage());
        } finally {
            $this->closeDb();
        }
    }
}