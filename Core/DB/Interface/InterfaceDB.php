<?php

namespace Core\DB\Interface;

interface InterfaceDB{

    function connect(string $driver,string $host, string $dbName, string $username, string $password, array $option);

    function closeDb();

}