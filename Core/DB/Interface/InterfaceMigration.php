<?php

namespace Core\DB\Interface;

interface InterfaceMigration{
    public static function migration() : string;
}