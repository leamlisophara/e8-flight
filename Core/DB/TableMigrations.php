<?php

namespace Core\DB;

use Core\DB\Interface\InterfaceMigration;

class Table_Migrations implements InterfaceMigration{
    private static $table = 'table_migrations';
    public static function migration(): string
    {
        return 'CREATE TABLE '. self::$table .' (
            id serial primary key,
            file varchar(255) not null,
            migrated_at date default CURRENT_DATE
        )';
    }

    public static function migratedFile(){
        return 'SELECT file FROM '. self::$table;
    }
}