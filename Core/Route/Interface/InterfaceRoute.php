<?php

namespace Core\Route\Interface;

interface InterfaceRoute
{
    public function get($path, $callback);
    public function post($path, $callback);
    public function put($path, $callback);
    public function delete($path, $callback);

    public function execute();

}