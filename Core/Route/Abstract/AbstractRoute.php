<?php

namespace Core\Route\Abstract;

use Closure;
use Core\Request;
use Core\Route\Interface\InterfaceRoute;
use Core\Route\Route;
use Core\Route\Test;
use Exception;
use ReflectionClass;
use ReflectionFunction;
use ReflectionMethod;

class AbstractRoute implements InterfaceRoute
{

    private $middleware;
    private $name;

    protected $route;

    private $method;
    private $current_path = null;

    private function routeConstruction($path, Closure|string $callback, $method){
        // preg leam/{lisophara} | leam/lisophara | leam/{li}/sophara  | {leamlisophara}
        $match = preg_match_all('/^([a-zA-Z\/-]+\{[a-zA-Z]+\}|[a-zA-Z\/-]+|[a-zA-Z\/-]+\{[a-zA-Z]+\}\/[a-zA-Z\/-]+|\{[a-zA-Z]+\}){1,}$/', $path);
        if($match == false){
            throw new Exception('URL in correct format');
        }
        $method = strtolower($method);
        $httpMethod = in_array($method, ['get', 'post', 'put', 'delete']) ? $method : throw new \Exception('Invalid Http method');
        $p = strtolower($path);
        $this->route[$httpMethod][] = [
            'path' => $p,
            'callback' => $callback,
        ];
        $this->method = $method;
        $this->current_path = $p;
    }

    private function extend(string $key, $value, $httpMethod){
        $methodIndex = count($this->route[strtolower($httpMethod)]) - 1;
        $this->route[$httpMethod][$methodIndex][$key] = $value;
    }

    public function get($path, $callback)
    {
        $this->routeConstruction($path, $callback, 'GET');
        return $this;
    }

    public function post($path, $callback)
    {
        // TODO: Implement post() method.
        $this->routeConstruction($path, $callback, 'POST');
        return $this;
    }

    public function put($path, $callback)
    {
        // TODO: Implement put() method.
        $this->routeConstruction($path, $callback, 'PUT');
        return $this;
    }

    public function delete($path, $callback)
    {
        // TODO: Implement delete() method.
        $this->routeConstruction($path, $callback, 'DELETE');
        return $this;
    }

    public function name(string $name){
        if($this->search($name, $this->route)){
            throw new \Exception('Route name already exist!');
        }
        $this->extend('name', $name, $this->method);
        return $this;
    }

    public function prefix(string $prefixPath){
        $path = $this->search($this->current_path, $this->route);

        if($path === false){
            throw new \Exception('Please provide path first!');
        }
        $currentArray = count($this->route[$this->method]) - 1;
        $path = $this->route[$this->method][$currentArray]['path'];
        $prefix = trim($prefixPath, '/').'/'.$path;
        $this->extend('path', $prefix, $this->method);
        return $this;
    }

    public function middleware(array $middleware){
        $this->extend('middleware', $middleware, $this->method);
        return $this;
    }

    public function execute()
    {
        $req = new Request();
        $method = $_SERVER['REQUEST_METHOD'];
        if( ! in_array($method, ['GET', 'POST', 'PUT', 'DELETE']) ){
            exit;
        }
        $args = [];
        $uri = explode('?', $_SERVER['REQUEST_URI'], 2)[0];
        // $route = $this->findRoute($uri, $method);
        $routes = $this->route[strtolower($method)];
        $route = false;
        $uri = strtolower($uri);
        foreach($routes as $tmp){
            if( strtolower($tmp['path']) === $uri){
                $route = $tmp;
                break;
            }
        }

        if($route !== false){
            try {
                if($route['callback'] instanceof \Closure){
                    if(!is_callable($route['callback'])){
                        throw new Exception('Funtion not found!');
                    }
                    $args = $this->injection($route['callback'], $req);
                    echo call_user_func_array($route['callback'], $args);
                } else if (strpos($route['callback'], ':') !== false){
                    $split = explode(':', $route['callback'], 2);                    
                    $className = 'Src\Controllers\\' . $split[0];
                    $args = $this->injection($split[1], $req, $className );
                    $class = new $className();
                    echo call_user_func_array([$class, $split[1]], $args);            
                }else {
                    echo 'Route not found!';
                    exit;
                }
            } catch (\Exception $e) {
                header('HTTP/1.0 500 Internal Server Error Dude! -> ' . $e->getMessage());
                var_dump($e->getMessage());
                exit;
            }
        }
    }

    private function injection($func, $args, $class = null){
        $req = new Request();
        if($class){
            if(! class_exists($class)){
                throw new Exception('Class not found!');
            }
            $i = 0;
            $argArr = [];
            $cl = new ReflectionClass($class);
            $params = $cl->getMethod($func)->getParameters();
            foreach($params as $index => $param){
                if($param->getType()->getName() === Request::class){
                    $argArr[] = $req;
                    continue;
                }
                $argArr[] = $args[$i] ;
                $i++;
            }
            return $argArr;
        }else{
            $argArr = [];
            $i = 0;
            $m = new ReflectionFunction($func);
            $params = $m->getParameters();
            foreach($params as $param){
                if($param->getType()->getName() === Request::class){
                    $argArr[] = $req;
                    continue;
                }
                $argArr[] = $args[$i];
                $i++;
            }
            return $argArr;
        }
    }

    private function search(string|int|bool $search, array $haystack, bool $inSensitiveCase = false) : string|int|false {
        $recursiveResult = false;
        foreach ($haystack as $key => $arr){
            if(is_array($arr)){
                $recursiveResult .= $this->search($search, $arr, $inSensitiveCase);
                if($recursiveResult){
                    return $recursiveResult;
                }
            }
            if($inSensitiveCase){
                if(strtolower($search) == strtolower($arr)){
                    return $key;
                }
            }else{
                if($search == $arr){
                    return $key;
                }
            }
        }
        return false;
    }

    private function getRecursiveKey(string|int|bool $search, array $haystack, string $currentKey = null, bool $inSensitiveCase = false){
        $result = false;
        $recursiveResult = false;
        foreach ($haystack as $key => $arr){
            if(is_array($arr)){
                $recursiveResult = trim($this->getRecursiveKey($search, $arr, $currentKey . '=>' . $key, $inSensitiveCase) . '=>', '=>');
                if($recursiveResult){
                    return $recursiveResult;
                }
            }
            if($inSensitiveCase){
                if(strtolower($search) == strtolower($arr)){
                    return $currentKey . '=>' . $key . '=>';
                }
            }else{
                if($search == $arr){
                    return $currentKey . '=>' . $key . '=>';
                }
            }
        }
        return false;
    }

    private function findRoute(string $uri, string $method) : array|bool{
        $routes = $this->route[strtolower($method)];
        $uri = strtolower($uri);
        foreach($routes as $route){
            if( strtolower($route['path']) === $uri){
                return $route;
            }
        }
        return false;
    }

    private function getArrayValue(string $routeName, string $key = null){
        $path = $this->getRecursiveKey($routeName, $this->route);
    }
}