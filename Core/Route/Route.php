<?php

namespace Core\Route;

use Core\Route\Abstract\AbstractRoute;

class Route extends AbstractRoute
{
    private static $instant_route = null;
    public static $t = 0;

    private function __construct(){}

    public static function route(string $name) : string|bool{
        $routes = self::$instant_route->route;
        foreach($routes as $route){
            foreach($route as $tmp){
                if( ($tmp['name'] ?? '') === $name){
                    return url($tmp['path']);
                }
            }
        }
        return false;
    }

    public static function getRoute(){
        self::$t++;
        if(self::$instant_route === null){
            self::$instant_route = new Route();
            return self::$instant_route;
        }else{
            return self::$instant_route;
        }
    }

    // public static function d(){
    //     return self::$instant_route->route;
    // }

}