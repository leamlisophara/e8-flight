<?php

namespace Core\Model;

use Core\DB\DB;
use Core\DB\QueryBuilder;
use PDO;

//use Core\Model\Abstract\AbstractModel;

class Model extends DB {

    protected $data_field = [];
    private $class;
    private QueryBuilder $builder;

    public function __construct()
    {
        $segment = explode('\\', get_called_class());
        $className = $segment[count($segment) - 1];
        $this->table = $this->table ?? snakeCase($className) . 's';
        $this->class = get_called_class();
        $this->builder = new QueryBuilder();
        $this->builder->from($this->table);
    }

    public function fill(array $data){
        $field = $this->columnAble($data);
        $class_string = $this->class;
        $class = new $class_string();
        foreach($field as $key => $value){
            $class->{$key} = $value;
        }
        return $class->getArray();
    }

    public function select(array $arr = ['*']){
        $this->builder->select($arr);
        return $this->builder;
    }

    public function get(){
        $query = $this->builder->getQuery();
        return $this->fetchModel($query);
    }

    public function insertData(array $array){
        $data = $this->columnAble($array);
        $result = $this->insert($data);
        return $result;
    }

    public function updateData(array $array, $callback){
        $data = $this->columnAble($array);
        $result = $this->update($data, $callback);
        return $result;
    }

    private function columnAble(array $array){
        $tmp = [];
        $this->data_field[] = 'id';
        foreach($this->data_field as $field){
            if(array_key_exists($field, $array)){
                $tmp[$field] = $array[$field];
            }
        }
        return $tmp;
    }

    public function save(){
        $id = $this->id ?? false;
        $result = null;
        if($id){
            $result = $this->updateData($this->getArray(), fn($q) => $q->where('id', $id));
        }else{
            $result = $this->insertData($this->getArray());
        }
        return $result;
    }

    public function getArray(){
        $tmp = [];
        foreach($this->data_field as $field){
            $tmp[$field] = $this->{$field};
        }
        return $tmp;
    }

    private function fetchModel(string $query){
        $this->connect(DB_DRIVER, DB_HOST, DB_NAME, DB_USERNAME, DB_PASSWORD, ['port' => DB_PORT]);
        $s = $this->connection->prepare($query);
        $s->execute();
        $result = $s->fetchAll(PDO::FETCH_ASSOC);
        $this->connection = null;
        if($result !== []){
            $return = [];
            foreach($result as $d){
                $return[] = $this->fill($d);
            }
            return $return;
        }
        return [];
    }

    public function getTableName(){
        return $this->table;
    }

}