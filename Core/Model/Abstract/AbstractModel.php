<?php

namespace Core\Model\Abstract;

use Core\DB\QueryBuilder;
use Core\Model\Interface\InterfaceModel;
use Exception;

class AbstractModel extends QueryBuilder implements InterfaceModel {
    protected $fields;

    public function __construct()
    {
        // Auto $table name
        $this->table = $this->table ?? snakeCase(class_basename(get_called_class())).'s';
    }

    public function setTable(string $table){
        $this->table = $table;
        return $this;
    }

    public function __get($name)
    {
        if(! in_array($name, $this->fields)){
            throw new Exception('Get a forbidden fields!');
        }
        return $this->fields->{$name} ?? $this->fields[$name];
    }

    public function __set($name, $value)
    {
        if(! in_array($name, $this->fields)){
            throw new Exception('Set unavailable field!');
        }
        $this->fields[$name] = $value;
    }

    public function save(){
        
    }

    public function create(array $collection){

    }


}