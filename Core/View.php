<?php
namespace Core;

use Exception;

class View{
    private static $view = null;
    private static $layout = null;
    private static $children = null;
    private static $name = null;

    private function __construct(){}

    public static function extends($layout){
        self::$layout = $layout;
    }

    public static function start($name){
        self::$name = $name;
        ob_start();
    }

    public static function end(){
        if(self::$name === null){
            throw new Exception('Can not end without start first!');
        }
        $tmp = ob_get_contents();
        ob_end_clean();

        self::$children[self::$name] = $tmp;
    }

    public static function pull_view($name){
        echo self::$children[$name];
        unset(self::$children[$name]);
    }

    public static function run(){
        echo view(self::$layout);
    }
}