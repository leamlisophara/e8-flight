<?php 
namespace Core;

use ArrayAccess;

class Request implements ArrayAccess{
    private $request;

    public function __construct()
    {
        $this->request = $_SERVER['REQUEST_METHOD'] === 'GET' ? $_GET : $_POST;
        foreach($this->request as $key => $req){
            $this->__set($key, $req);
        }
    }

    public function all(){
        return $this->request;
    }

    public function get($key, $default = null){
        $result = $this->{$key} ?? $default;
        return $result;
    }

    public function __get($name)
    {
        return $this->{$name} ?? null;
    }

    public function __set($name, $value)
    {
        $this->{$name} = $value;
    }

    public function offsetExists(mixed $offset): bool
    {
        return isset($this->$offset);
    }
    public function offsetGet($offset) {
        return $this->$offset ?? null;
    }

    public function offsetSet($offset , $value): void {
        $this->$offset = $value;
    }

    public function  offsetUnset(mixed $offset): void
    {
        unset($this->$offset);
    }
}