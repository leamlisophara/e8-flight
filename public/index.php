<?php

use Core\Route\Route;
session_start();
require __DIR__ . '/../vendor/autoload.php';
$RouteServices = include_once '../src/config/RouteServiceProvider.php';
foreach ($RouteServices as $service) {
    include_once $service;
}
$route = Route::getRoute();
$route->execute();